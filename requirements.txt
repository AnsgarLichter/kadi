alembic==1.11.1
anonip==1.1.0
Authlib==1.2.1
Babel==2.12.1
blinker==1.6.2
celery==5.3.1
charset-normalizer==3.2.0
click==8.1.4
cryptography==41.0.2
defusedxml==0.7.1
# Version 8 only works with Elasticsearch 8.
elasticsearch==7.17.9
elasticsearch-dsl==7.4.1
email-validator==2.0.0
Flask==2.3.2
Flask-Babel==3.0.0
Flask-Limiter==3.3.1
Flask-Login==0.6.2
Flask-Migrate==4.0.4
Flask-SQLAlchemy==3.0.5
flask-talisman==1.0.0
Flask-WTF==1.1.1
fpdf2==2.7.4
h5py==3.9.0
itsdangerous==2.1.2
Jinja2==3.1.2
ldap3==2.9.1
limits==3.5.0
markdown-it-py==3.0.0
MarkupSafe==2.1.3
marshmallow==3.19.0
mdit-py-plugins==0.4.0
mdurl==0.1.2
Pillow==10.0.0
pluggy==1.2.0
psycopg2==2.9.6
PyJWT==2.7.0
python-magic==0.4.27
qrcode==7.4.2
rdflib==6.3.2
redis==4.6.0
requests==2.31.0
rfc3987==1.3.8
sentry-sdk==1.28.0
# Version >=2 has a new query interface that requires larger refactorings.
SQLAlchemy==1.4.49
SQLAlchemy-Utils==0.41.1
uWSGI==2.0.21
Werkzeug==2.3.6
WTForms==3.0.1
zipstream-ng==1.6.0
