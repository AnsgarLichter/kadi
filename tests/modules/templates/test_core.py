# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.permissions.core import add_role
from kadi.modules.collections.core import update_collection
from kadi.modules.templates.core import create_template
from kadi.modules.templates.core import delete_template
from kadi.modules.templates.core import purge_template
from kadi.modules.templates.core import restore_template
from kadi.modules.templates.core import update_template
from kadi.modules.templates.models import Template
from kadi.modules.templates.models import TemplateState
from kadi.modules.templates.models import TemplateType
from kadi.modules.templates.models import TemplateVisibility


@pytest.mark.parametrize(
    "template_type,template_data",
    [(TemplateType.RECORD, {}), (TemplateType.EXTRAS, [])],
)
def test_create_template(template_type, template_data, dummy_user):
    """Test if templates are created correctly."""
    template = create_template(
        creator=dummy_user,
        type=template_type,
        data=template_data,
        identifier="test",
        title="test",
        description="# test",
        visibility=TemplateVisibility.PRIVATE,
    )

    assert Template.query.filter_by(identifier="test").one() == template
    assert template.plain_description == "test"
    assert dummy_user.roles.filter_by(
        name="admin", object="template", object_id=template.id
    ).one()


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_update_template(template_type, dummy_user, new_template):
    """Test if templates are updated correctly."""
    template = new_template(type=template_type)
    update_template(template, description="# test", user=dummy_user)

    assert template.plain_description == "test"
    assert template.revisions.count() == 2


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_delete_template(template_type, dummy_user, new_template):
    """Test if templates are deleted correctly."""
    template = new_template(type=template_type)
    delete_template(template, user=dummy_user)

    assert template.state == TemplateState.DELETED
    assert template.revisions.count() == 2


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_restore_template(template_type, dummy_user, new_template):
    """Test if templates are restored correctly."""
    template = new_template(type=template_type)

    delete_template(template, user=dummy_user)
    restore_template(template, user=dummy_user)

    assert template.state == TemplateState.ACTIVE
    assert template.revisions.count() == 3


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_purge_template(
    template_type, db, dummy_collection, dummy_user, new_template, new_user
):
    """Test if templates are purged correctly."""
    user = new_user()
    template = new_template(type=template_type)

    if template.type == TemplateType.RECORD:
        update_collection(
            dummy_collection, record_template_id=template.id, user=dummy_user
        )

    add_role(user, "template", template.id, "member")
    db.session.commit()

    prev_timestamp = dummy_collection.last_modified

    purge_template(template)

    assert Template.query.get(template.id) is None
    # Only the system role should remain.
    assert user.roles.count() == 1

    if template.type == TemplateType.RECORD:
        # Check if the collection was also updated and a new revision was created
        # without an associated user.
        assert dummy_collection.last_modified != prev_timestamp
        assert dummy_collection.revisions.count() == 3
        assert dummy_collection.ordered_revisions.first().revision.user is None
