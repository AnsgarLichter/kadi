# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.modules.templates.forms import NewExtrasTemplateForm
from kadi.modules.templates.forms import NewRecordTemplateForm
from kadi.modules.templates.models import TemplateType


def test_new_record_template_form_template(
    dummy_collection,
    dummy_license,
    dummy_record,
    dummy_user,
    new_collection,
    new_template,
    new_user,
):
    """Test if prefilling a "NewRecordTemplateForm" with a template works correctly."""

    # This collection should not appear in the linked collections in the form.
    collection = new_collection(creator=new_user())

    template = new_template(
        data={
            "identifier": "test",
            "type": "test",
            "license": dummy_license.name,
            "tags": ["test"],
            "collections": [dummy_collection.id, collection.id],
            "roles": [],
        }
    )
    form = NewRecordTemplateForm(template=template, user=dummy_user)

    template_type = template.data["type"]
    tag = template.data["tags"][0]

    assert form.identifier.data == template.identifier
    assert form.record_identifier.data == template.data["identifier"]
    assert form.record_type.initial == (template_type, template_type)
    assert form.record_license.initial == (dummy_license.name, dummy_license.title)
    assert form.record_tags.initial == [(tag, tag)]
    assert form.record_collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]
    assert form.record_roles.initial == []


def test_new_record_template_form_record(
    dummy_license, dummy_collection, dummy_user, new_collection, new_record, new_user
):
    """Test if prefilling a "NewRecordTemplateForm" with a record works correctly."""
    record = new_record(
        type="test",
        license=dummy_license.name,
        tags=["test"],
        extras=[{"type": "str", "key": "test", "value": "test"}],
    )

    user = new_user()
    # This collection should not appear in the linked collections in the form.
    collection = new_collection(creator=user)
    add_link(record.collections, collection, user=user)
    add_link(record.collections, dummy_collection, user=dummy_user)

    form = NewRecordTemplateForm(record=record, user=dummy_user)

    tag = record.tags.first().name

    assert form.record_identifier.data == record.identifier
    assert form.record_type.initial == (record.type, record.type)
    assert form.record_license.initial == (dummy_license.name, dummy_license.title)
    assert form.record_tags.initial == [(tag, tag)]
    assert form.record_extras.data == [{"type": "str", "key": "test", "value": None}]
    assert form.record_collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]
    assert form.record_roles.initial == []


def test_new_extras_template_form_template(new_template):
    """Test if prefilling a "NewExtrasTemplateForm" with a template works correctly."""
    template = new_template(
        type=TemplateType.EXTRAS, data=[{"type": "str", "key": "test", "value": "test"}]
    )
    form = NewExtrasTemplateForm(template=template)

    assert form.identifier.data == template.identifier
    assert form.extras.data == template.data


def test_new_extras_template_form_record(dummy_record):
    """Test if prefilling a "NewExtrasTemplateForm" with a record works correctly."""
    dummy_record.extras = [{"type": "str", "key": "test", "value": "test"}]
    form = NewExtrasTemplateForm(record=dummy_record)

    assert form.extras.data == [{"type": "str", "key": "test", "value": None}]
