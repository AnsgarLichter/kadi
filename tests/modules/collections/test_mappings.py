# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.collections.mappings import CollectionMapping


def test_collection_mapping(dummy_collection):
    """Test if the mapping for collections works correctly."""
    doc = CollectionMapping.create_document(dummy_collection)

    assert doc.meta.id == dummy_collection.id
    assert doc.identifier == dummy_collection.identifier
