# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from sqlalchemy.exc import IntegrityError

from kadi.modules.records.models import Chunk
from kadi.modules.records.models import FileState


def test_file_uniqueness(db, dummy_file, new_file):
    """Test if the conditional unique constraint of files works correctly."""
    file = new_file(state=FileState.INACTIVE)

    file.name = dummy_file.name
    db.session.commit()

    with pytest.raises(IntegrityError):
        file.state = FileState.ACTIVE
        db.session.commit()


def test_chunk_update_or_create(dummy_upload):
    """Test if updating or creating a chunk works correctly."""
    chunk1 = Chunk.update_or_create(upload=dummy_upload, index=0, size=0)
    chunk2 = Chunk.update_or_create(upload=dummy_upload, index=0, size=1)

    assert chunk1 == chunk2
    assert chunk1.size == chunk2.size == 1
