# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.storage.core import get_storage
from kadi.modules.records.api.utils import check_file_exists
from kadi.modules.records.api.utils import check_storage_compatibility
from kadi.modules.records.api.utils import check_upload_user_quota


def test_check_file_exists(dummy_record, dummy_file):
    """Test if checking for existing files works correctly."""
    response, file = check_file_exists(dummy_record, dummy_file.name)

    assert response is not None
    assert file == dummy_file

    response, file = check_file_exists(dummy_record, "test")

    assert response is None
    assert file is None


def test_check_storage_compatibility(dummy_file):
    """Test if checking for storage compatibility works correctly."""
    response = check_storage_compatibility(dummy_file.storage, get_storage("local"))
    assert response is None


@pytest.mark.parametrize(
    "quota,size,quota_met",
    [
        (0, 0, True),
        (5, 0, True),
        (5, 5, True),
        (0, -5, True),
        (-5, -5, True),
        (-5, 0, False),
        (0, 5, False),
    ],
)
def test_check_upload_user_quota(
    quota, size, quota_met, monkeypatch, dummy_file, dummy_user
):
    """Test if checking for upload user quotas works correctly."""
    monkeypatch.setitem(
        current_app.config, "UPLOAD_USER_QUOTA", dummy_file.size + quota
    )

    response = check_upload_user_quota(user=dummy_user, additional_size=size)

    if quota_met:
        assert response is None
    else:
        assert response is not None
