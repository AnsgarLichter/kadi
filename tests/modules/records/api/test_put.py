# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from .utils import initiate_upload
from .utils import upload_chunk
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_edit_file_data(api_client, dummy_file, dummy_personal_token, dummy_record):
    """Test the "api.edit_file_data" endpoint."""
    response = initiate_upload(
        api_client(dummy_personal_token),
        url_for("api.edit_file_data", record_id=dummy_record.id, file_id=dummy_file.id),
        file_data=10 * b"x",
        replace_file=True,
    )
    check_api_response(response, status_code=201)


@pytest.mark.parametrize(
    "chunk_size,chunk_checksum,status_code",
    [
        (None, None, 200),
        # Size mismatch.
        (10, None, 400),
        # Checksum mismatch.
        (None, "test", 400),
    ],
)
def test_upload_chunk(
    chunk_size,
    chunk_checksum,
    status_code,
    monkeypatch,
    api_client,
    dummy_personal_token,
    dummy_record,
):
    """Test the "api.upload_chunk" endpoint."""
    client = api_client(dummy_personal_token)

    response = initiate_upload(client, url_for("api.new_upload", id=dummy_record.id))
    data = response.get_json()

    response = upload_chunk(
        client,
        data["_actions"]["upload_chunk"],
        size=chunk_size,
        checksum=chunk_checksum,
    )
    check_api_response(response, status_code=status_code)
