# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_get_system_information(client, dummy_user, user_session):
    """Test the internal "api.get_system_information" endpoint."""
    dummy_user.is_sysadmin = True

    with user_session():
        response = client.get(url_for("api.get_system_information"))
        check_api_response(response)
