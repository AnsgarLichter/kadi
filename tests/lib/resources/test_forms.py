# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from werkzeug.datastructures import MultiDict

from kadi.lib.forms import KadiForm
from kadi.lib.permissions.core import add_role
from kadi.lib.resources.forms import RolesField
from kadi.lib.resources.forms import TagsField


@pytest.mark.parametrize(
    "data,is_valid,result,error",
    [
        (None, True, [], None),
        ({"test": " A "}, True, ["a"], None),
        ({"test": [" B ", "a", "c   d", "A"]}, True, ["a", "b", "c d"], None),
        ({"test": ["abcd"]}, False, [], "Tags cannot be longer than 3 characters."),
        ({"test": [" "]}, False, [], "Tags must not be empty."),
    ],
)
def test_tags_field(data, is_valid, result, error):
    """Test if the custom "TagsField" works correctly."""

    class _TestForm(KadiForm):
        test = TagsField("Test", max_len=3)

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result

    if not is_valid:
        assert error in form.errors["test"]


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, []),
        ({"test": "[]"}, True, []),
        (
            {"test": '[{"subject_type": "user", "subject_id": 1, "role": "role"}]'},
            True,
            [{"subject_type": "user", "subject_id": 1, "role": "role"}],
        ),
        (
            {"test": '[{"subject_type": "user", "subject_id": 1, "role": null}]'},
            True,
            [{"subject_type": "user", "subject_id": 1, "role": None}],
        ),
        (
            {"test": '[{"subject_type": "user", "subject_id": 1, "role": "test"}]'},
            False,
            [],
        ),
        ({"test": '[{"foo": "bar"}]'}, False, []),
    ],
)
def test_roles_field_submit(data, is_valid, result):
    """Test if submitting the custom "RolesField" works correctly."""

    class _TestForm(KadiForm):
        test = RolesField("Test", roles=[("role", "Title")])

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result

    if not is_valid:
        assert "Invalid data structure." in form.errors["test"]


def test_roles_field_prefill(
    dummy_group, dummy_record, dummy_user, new_group, new_user
):
    """Test if prefilling the custom "RolesField" works correctly."""

    class _TestForm(KadiForm):
        test = RolesField("Test", roles=[("role", "Title")])

    form = _TestForm(formdata=MultiDict())

    user = new_user()
    # Not readable by the dummy user.
    group = new_group(creator=user)
    add_role(user, "record", dummy_record.id, "member")
    add_role(dummy_group, "record", dummy_record.id, "member")
    add_role(group, "record", dummy_record.id, "member")

    data = [
        {"subject_type": "user", "subject_id": user.id, "role": "member"},
        {"subject_type": "group", "subject_id": dummy_group.id, "role": "member"},
        {"subject_type": "group", "subject_id": group.id, "role": "member"},
    ]

    initial_data = [
        {
            "subject_type": "user",
            "subject": [user.id, f"@{user.identity.username}"],
            "role": "member",
        },
        {
            "subject_type": "group",
            "subject": [dummy_group.id, f"@{dummy_group.identifier}"],
            "role": "member",
        },
    ]

    form.test.set_initial_data(data=data, user=dummy_user)
    assert form.test.initial == initial_data

    form.test.set_initial_data(resource=dummy_record, user=dummy_user)
    assert form.test.initial == initial_data
