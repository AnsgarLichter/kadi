# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app
from flask import json

from kadi.lib.config.core import get_sys_config
from kadi.lib.config.core import get_user_config
from kadi.lib.config.core import MISSING
from kadi.lib.config.core import remove_sys_config
from kadi.lib.config.core import remove_user_config
from kadi.lib.config.core import set_sys_config
from kadi.lib.config.core import set_user_config
from kadi.lib.config.models import ConfigItem
from kadi.lib.db import KadiAesEngine


def test_config_item_update_or_create(dummy_user):
    """Test if updating or creating a config item works correctly."""
    sys_config_item_1 = ConfigItem.update_or_create(key="FOO", value="BAR")
    sys_config_item_2 = ConfigItem.update_or_create(key="FOO", value="BAZ")

    assert sys_config_item_1 == sys_config_item_2
    assert sys_config_item_1.value == sys_config_item_2.value == "BAZ"

    user_config_item_1 = ConfigItem.update_or_create(
        key="FOO", value="BAR", user=dummy_user
    )
    user_config_item_2 = ConfigItem.update_or_create(
        key="FOO", value="BAZ", user=dummy_user
    )

    assert user_config_item_1 == user_config_item_2
    assert user_config_item_1.value == user_config_item_2.value == "BAZ"


def test_get_sys_config(monkeypatch):
    """Test if retrieving global config items works correctly."""

    # Config item does neither exist in the application config nor database.
    assert get_sys_config("FOO", use_fallback=True) is MISSING
    assert get_sys_config("FOO", use_fallback=False) is MISSING

    # Config item does exist in the application config.
    monkeypatch.setitem(current_app.config, "FOO", "BAR")

    assert get_sys_config("FOO", use_fallback=True) == "BAR"
    assert get_sys_config("FOO", use_fallback=False) is MISSING

    # Config item does also exist in the database.
    ConfigItem.create(key="FOO", value="BAZ")

    assert get_sys_config("FOO", use_fallback=True) == "BAZ"
    assert get_sys_config("FOO", use_fallback=False) == "BAZ"


def test_set_sys_config(db):
    """Test if setting global config items works correctly."""

    # Key does not exist in the config.
    assert set_sys_config("FOO", "BAR") is None

    # Key does exist in the config.
    assert set_sys_config("TESTING", False) is not None

    db.session.commit()

    assert ConfigItem.query.filter_by(key="TESTING").first().value is False

    # Value is equal to the default config value.
    assert set_sys_config("TESTING", True) is None

    db.session.commit()

    assert not ConfigItem.query.all()


def test_remove_sys_config(db):
    """Test if removing global config items works correctly."""
    assert remove_sys_config("FOO") is False

    ConfigItem.create(key="FOO", value="BAR")
    db.session.commit()

    assert remove_sys_config("FOO") is True

    db.session.commit()

    assert not ConfigItem.query.all()


@pytest.mark.parametrize("decrypt", [False, True])
def test_get_user_config(decrypt, dummy_user):
    """Test if retrieving user-specific config items works correctly."""
    assert get_user_config("FOO", user=dummy_user, decrypt=decrypt) is MISSING
    assert (
        get_user_config("FOO", user=dummy_user, default="BAR", decrypt=decrypt) == "BAR"
    )

    test_value = "BAZ"

    # Save either a regular or an encrypted config value.
    config_value = (
        test_value
        if not decrypt
        else KadiAesEngine.create().encrypt(json.dumps(test_value))
    )
    ConfigItem.create(key="FOO", value=config_value, user=dummy_user)

    assert get_user_config("FOO", user=dummy_user, decrypt=decrypt) == test_value

    # When decrypting, also check whether decryption errors are caught correctly.
    if decrypt:
        ConfigItem.update_or_create(key="FOO", value=test_value, user=dummy_user)
        assert get_user_config("FOO", user=dummy_user, decrypt=decrypt) is MISSING


@pytest.mark.parametrize("encrypt", [False, True])
def test_set_user_config(encrypt, dummy_user):
    """Test if setting user-specific config items works correctly."""
    test_value = "BAR"

    assert set_user_config("FOO", test_value, user=dummy_user, encrypt=encrypt)

    # Use either a regular or an encrypted config value for the assertion.
    config_value = (
        test_value
        if not encrypt
        else KadiAesEngine.create().encrypt(json.dumps(test_value))
    )

    assert (
        ConfigItem.query.filter_by(key="FOO", user_id=dummy_user.id).first().value
        == config_value
    )


def test_remove_user_config(db, dummy_user):
    """Test if removing user-specific config items works correctly."""
    assert remove_user_config("FOO", user=dummy_user) is False

    ConfigItem.create(key="FOO", value="BAR", user=dummy_user)
    db.session.commit()

    assert remove_user_config("FOO", user=dummy_user) is True

    db.session.commit()

    assert not ConfigItem.query.all()
