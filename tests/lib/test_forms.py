# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import timezone

import pytest
from werkzeug.datastructures import MultiDict
from wtforms.validators import StopValidation

from kadi.lib.config.core import get_sys_config
from kadi.lib.config.core import get_user_config
from kadi.lib.config.models import ConfigItem
from kadi.lib.forms import BaseConfigForm
from kadi.lib.forms import check_duplicate_identifier
from kadi.lib.forms import DynamicMultiSelectField
from kadi.lib.forms import DynamicSelectField
from kadi.lib.forms import JSONField
from kadi.lib.forms import KadiForm
from kadi.lib.forms import LFTextAreaField
from kadi.lib.forms import StringField
from kadi.lib.forms import UTCDateTimeField
from kadi.modules.records.models import Record


@pytest.mark.parametrize(
    "data,is_valid,result",
    [(None, True, None), ({"test": "1"}, True, 1), ({"test": "test"}, False, None)],
)
def test_dynamic_select_field(data, is_valid, result):
    """Test if the custom "DynamicSelectField" works correctly."""

    class _TestForm(KadiForm):
        test = DynamicSelectField("Test", coerce=int)

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, []),
        ({"test": ["1", "2"]}, True, [1, 2]),
        ({"test": ["1", "test"]}, False, []),
    ],
)
def test_dynamic_multi_select_field(data, is_valid, result):
    """Test if the custom "DynamicMultiSelectField" works correctly."""

    class _TestForm(KadiForm):
        test = DynamicMultiSelectField("Test", coerce=int)

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result


@pytest.mark.parametrize(
    "data,result",
    [
        (None, None),
        ({"test": ""}, ""),
        ({"test": "a\nb"}, "a\nb"),
        ({"test": "a\r\nb"}, "a\nb"),
    ],
)
def test_lf_text_area_field(data, result):
    """Test if the custom "LFTextAreaField" works correctly."""

    class _TestForm(KadiForm):
        test = LFTextAreaField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate()
    assert form.test.data == result


@pytest.mark.parametrize(
    "data,is_valid",
    [
        (None, True),
        ({"test": "test"}, False),
        ({"test": "2020-01-01T00:00:00.000Z"}, True),
    ],
)
def test_utc_date_time_field(data, is_valid):
    """Test if the custom "UTCDateTimeField" works correctly."""

    class _TestForm(KadiForm):
        test = UTCDateTimeField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid

    if form.test.data is not None:
        assert form.test.data.tzinfo == timezone.utc


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, None),
        ({"test": "[]"}, True, []),
        ({"test": "["}, False, None),
    ],
)
def test_json_field(data, is_valid, result):
    """Test if the custom "JSONField" works correctly."""

    class _TestForm(KadiForm):
        test = JSONField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result

    if not is_valid:
        assert "Invalid JSON data." in form.errors["test"]


@pytest.mark.parametrize("suffix", [None, "test"])
def test_kadi_form(suffix):
    """Test if the custom form base class works correctly."""

    class _TestForm(KadiForm):
        test = StringField()

    form = _TestForm(suffix=suffix)

    assert (
        form.test.id
        == form.test.label.field_id
        == ("test" if suffix is None else f"test_{suffix}")
    )


@pytest.mark.parametrize("user_fixture", [None, "dummy_user"])
def test_base_config_form(user_fixture, get_fixture):
    """Test if the config form base class works correctly."""
    user = get_fixture(user_fixture) if user_fixture is not None else None

    ConfigItem.create(key="FOO", value="bar", user=user)
    ConfigItem.create(key="TESTING", value="true", user=user)

    class _TestForm(BaseConfigForm):
        foo = StringField()  # pylint: disable=disallowed-name

        # Only this field name is actually valid for the global config.
        testing = StringField()

    # Test prefilling of the form data.
    form = _TestForm(user=user)

    assert form.foo.data == "bar"
    assert form.testing.data == "true"

    # Test prefilling of the form data when using a key prefix, which is invalid in this
    # case.
    form = _TestForm(user=user, key_prefix="test")

    assert form.foo.data is None
    assert form.testing.data is None

    # Test prefilling of the form data when ignoring one of the fields.
    form = _TestForm(user=user, ignored_fields={"foo"})

    assert form.foo.data is None
    assert form.testing.data == "true"

    # Test actually setting the config values based on some form data. Also try to use
    # an encrypted value for one of the fields.
    form = _TestForm(
        user=user,
        encrypted_fields={"foo"},
        formdata=MultiDict({"foo": "baz", "testing": "false"}),
    )
    form.set_config_values()

    if user is None:
        assert get_sys_config("FOO", use_fallback=False) == "bar"
        assert get_sys_config("TESTING", use_fallback=False) == "false"
    else:
        assert get_user_config("FOO", user=user, decrypt=True) == "baz"
        assert get_user_config("TESTING", user=user) == "false"


def test_check_duplicate_identifier(dummy_record):
    """Test if checking for a duplicate identifier works correctly."""
    check_duplicate_identifier(Record, "test")
    check_duplicate_identifier(Record, dummy_record.identifier, exclude=dummy_record)

    with pytest.raises(StopValidation):
        check_duplicate_identifier(Record, dummy_record.identifier)
